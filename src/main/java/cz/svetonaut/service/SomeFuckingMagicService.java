package cz.svetonaut.service;

import cz.svetonaut.messaging.ISender;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class SomeFuckingMagicService {

    @Inject
    private ISender jmsSender;

    public void doMagic(String text) {
        String msg = text.toUpperCase();
        jmsSender.sendMessage(msg);
    }
}
