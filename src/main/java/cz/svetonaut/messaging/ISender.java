package cz.svetonaut.messaging;

public interface ISender {

    void sendMessage(String text);
}
