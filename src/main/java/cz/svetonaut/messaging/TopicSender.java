package cz.svetonaut.messaging;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Topic;

@cz.svetonaut.messaging.topic.Topic
public class TopicSender implements ISender {

    @Inject
    @JMSConnectionFactory("jms/__defaultConnectionFactory")
    private JMSContext jmsContext;

    @Resource(lookup = "jms/msgTopic")
    private Topic topic;

    @Override
    public void sendMessage(String text) {
        jmsContext.createProducer().send(topic, text);
    }
}
