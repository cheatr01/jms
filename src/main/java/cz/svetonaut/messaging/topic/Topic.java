package cz.svetonaut.messaging.topic;

import java.lang.annotation.*;
import javax.enterprise.inject.Default;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Qualifier;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface Topic {

    public static final String NAME = "topic";

    public static final class Literal extends AnnotationLiteral<Topic> implements Topic {
        public static final Topic.Literal INSTANCE = new Topic.Literal();
        private static final long serialVersionUID = 1L;

        public Literal() {
        }
    }
}
