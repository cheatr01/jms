package cz.svetonaut.messaging.topic;

import cz.svetonaut.controllers.HomeController;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.*;

@MessageDriven(name = "secondTopicListener", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/msgTopic"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
        @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "msgTopic")
})
public class SecondMessageTopicListener implements MessageListener {

    @Inject
    private HomeController homeController;


    public void onMessage(Message message) {
        try {
            TextMessage textMessage = (TextMessage) message;
            String text = textMessage.getText();
            homeController.addMessageToSecondTopicList(text);
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
