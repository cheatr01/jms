package cz.svetonaut.messaging.topic;

import cz.svetonaut.controllers.HomeController;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.*;

@MessageDriven(name = "firstTopicListener", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/msgTopic"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
        @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "msgTopic")

})
public class FirstMessageTopicListener implements MessageListener{

    @Resource(lookup = "jms/__defaultConnectionFactory")
    private TopicConnectionFactory topicConnectionFactory;

    @Resource(lookup = "jms/msgTopic")
    private Topic topic;

    @Inject
    private HomeController homeController;

    /*public void receiveMessage() {
        try (TopicConnection topicConnection = topicConnectionFactory.createTopicConnection()) {
            TopicSession topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageConsumer consumer = topicSession.createConsumer(topic);
            Message receive = consumer.receive();

            TopicSubscriber subscriber = topicSession.createSubscriber(topic);
            subscriber.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {
                        homeController.addMessageToFirstTopicList(((TextMessage) message).getText());
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onMessage(Message message) {
        try {
            homeController.addMessageToFirstTopicList(((TextMessage) message).getText());
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
