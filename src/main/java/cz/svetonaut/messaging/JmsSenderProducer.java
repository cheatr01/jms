package cz.svetonaut.messaging;

import cz.svetonaut.controllers.HomeController;
import cz.svetonaut.messaging.queue.Queue;
import cz.svetonaut.messaging.topic.Topic;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

public class JmsSenderProducer {

    @Inject
    private HomeController homeController;

    @Inject @Topic
    private TopicSender topicSender;

    @Inject @Queue
    private QueueSender queueSender;

    @Inject
    @Any
    private Instance<ISender> senders;

//    @Produces
//    @RequestScoped
    public ISender producesSender() {
        String option = homeController.getOption();
        if (Topic.NAME.equalsIgnoreCase(option)) {
            return topicSender;
        } else if (Queue.NAME.equalsIgnoreCase(option)) {
            return queueSender;
        }else {
            throw new IllegalStateException("We build wall against this fucking illegals");
        }
    }

    @Produces
    @RequestScoped
    public ISender producesSenderFromInstance() {
        String option = homeController.getOption();
        if (Topic.NAME.equalsIgnoreCase(option)) {
            return senders.select(Topic.Literal.INSTANCE).get();
        } else if (Queue.NAME.equalsIgnoreCase(option)) {
            return senders.select(Queue.Literal.INSTANCE).get();
        }else {
            throw new IllegalStateException("We build wall against this fucking illegals");
        }
    }

    public ISender getSender(String option) {
        if (Topic.NAME.equalsIgnoreCase(option)) {
            return topicSender;
        } else if (Queue.NAME.equalsIgnoreCase(option)) {
            return queueSender;
        }else {
            throw new IllegalStateException("We build wall against this fucking illegals");
        }
    }

}
