package cz.svetonaut.messaging;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.*;

@cz.svetonaut.messaging.queue.Queue
public class QueueSender implements ISender {

    @Inject
    @JMSConnectionFactory("jms/__defaultConnectionFactory")
    private JMSContext jmsContext;

    @Resource(lookup = "jms/msgQueue")
    private Queue queue;

    @Override
    public void sendMessage(String message) {
        jmsContext.createProducer().send(queue, message);
    }

}
