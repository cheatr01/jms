package cz.svetonaut.messaging.queue;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Qualifier;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface Queue {

    public static final String NAME = "queue";

    public static final class Literal extends AnnotationLiteral<cz.svetonaut.messaging.queue.Queue> implements cz.svetonaut.messaging.queue.Queue{
        public static final cz.svetonaut.messaging.queue.Queue.Literal INSTANCE = new cz.svetonaut.messaging.queue.Queue.Literal();
        private static final long serialVersionUID = 1L;

        public Literal() {
        }
    }
}
