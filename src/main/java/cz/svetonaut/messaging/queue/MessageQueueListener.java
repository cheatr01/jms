package cz.svetonaut.messaging.queue;

import cz.svetonaut.controllers.HomeController;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven(name = "msgListener", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/msgQueue"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class MessageQueueListener implements MessageListener {

    @Inject
    private HomeController homeController;

    @Override
    public void onMessage(Message message) {
        try {
            TextMessage textMessage = (TextMessage) message;
            String text = textMessage.getText();
            homeController.addMessageToQueueList(text);
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
