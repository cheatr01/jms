package cz.svetonaut.controllers;

import cz.svetonaut.messaging.queue.Queue;
import cz.svetonaut.messaging.topic.Topic;
import cz.svetonaut.service.SomeFuckingMagicService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@ApplicationScoped
@Named
public class HomeController {

    private String inputText;
    private String option = Queue.NAME;
    private List<String> optionValues = new ArrayList<>(Arrays.asList(Queue.NAME, Topic.NAME));

    private List<String> queueMsgs = new ArrayList<>();
    private List<String> secondQueueMsgs = new ArrayList<>();
    private List<String> firstTopicMsgs = new ArrayList<>();
    private List<String> secondTopicMsgs = new ArrayList<>();

    @EJB
    private SomeFuckingMagicService magicService;

    public void sendMsg() {
        magicService.doMagic(inputText);
        inputText = null;
    }

    public void addMessageToQueueList(String message) {
        queueMsgs.add(message);
    }

    public void addMessageToSecondQueueList(String message) {
        secondQueueMsgs.add(message);
    }

    public void addMessageToFirstTopicList(String message) {
        firstTopicMsgs.add(message);
    }

    public void addMessageToSecondTopicList(String message) {
        secondTopicMsgs.add(message);
    }

    public List<String> getQueueMsgs() {
        return queueMsgs;
    }

    public void setQueueMsgs(List<String> queueMsgs) {
        this.queueMsgs = queueMsgs;
    }

    public String getInputText() {
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText = inputText;
    }

    public List<String> getSecondQueueMsgs() {
        return secondQueueMsgs;
    }

    public void setSecondQueueMsgs(List<String> secondQueueMsgs) {
        this.secondQueueMsgs = secondQueueMsgs;
    }

    public List<String> getFirstTopicMsgs() {
        return firstTopicMsgs;
    }

    public void setFirstTopicMsgs(List<String> firstTopicMsgs) {
        this.firstTopicMsgs = firstTopicMsgs;
    }

    public List<String> getSecondTopicMsgs() {
        return secondTopicMsgs;
    }

    public void setSecondTopicMsgs(List<String> secondTopicMsgs) {
        this.secondTopicMsgs = secondTopicMsgs;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public List<String> getOptionValues() {
        return optionValues;
    }

    public void setOptionValues(List<String> optionValues) {
        this.optionValues = optionValues;
    }
}
