# Build
mvn clean package && docker build -t cz.svetonaut/JMS .

# RUN

docker rm -f JMS || true && docker run -d -p 8080:8080 -p 4848:4848 --name JMS cz.svetonaut/JMS 